#!/bin/bash

apt update -y
apt upgrade -y
apt-get install net-tools

echo "[ -- Configuring DateTime with systemd]"
apt install systemd
timedatectl set-timezone America/Sao_Paulo

# Se o ubuntu precisar ser atualizado
# sudo do-release-upgrade -d
# sudo reboot

echo "[ -- Installing nodejs v16.13.1]"
sudo curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -
apt install nodejs -y

echo "[ -- Updating NPM 8.3.1 to 8.5.0]"
npm install -g npm@8.5.0
npm install -g npm@8.5.0

echo "[ -- Add NPM serve package v13.0.2]"
npm install -g serve@13.0.2

# echo "[ -- Installing NGINX v1.18.0]"
# apt install nginx=1.18.* -y

echo "[ -- Criando diretórios de trabalho /opt/zap-chat]"
mkdir /opt/zap-chat

echo "[ -- Setando permissões 777 para os diretórios de trabalho /opt/zap-chat]"
chmod -R 777 /opt/zap-chat

echo "[ -- Criando Verificador de Status de Daemon para os Services]"
{
    echo '#!/bin/bash'
    echo 'SERVICE=$1;'
    echo 'SERVICE_STATUS=$(echo $(sudo systemctl is-active ${SERVICE}));'
    echo 'if [ "inactive" = "${SERVICE_STATUS}" ]; then'
    echo '  echo "[ -- Service ${SERVICE} is inactive, nothing to do];";'
    echo 'else {'
    echo '  sudo systemctl stop ${SERVICE};'
    echo '  echo "[ -- The Service ${SERVICE} were stopped!]";'
    echo '};'
    echo 'fi'
} >> /opt/zap-chat/verificador-stop-daemon-service.sh

echo "[ -- Criando Daemon para zap-chat Service]"
{
    echo '[Unit]'
    echo 'Description=UI Service from zap-chat Platform'
    echo 'After=syslog.target'
    echo '\n'
    echo '[Service]'
    echo 'User=ubuntu'
    echo 'WorkingDirectory=/opt/zap-chat'
    echo 'StandardOutput=journal'
    echo 'StandardError=journal'
    echo 'SyslogIdentifier=zap-chat'
    echo 'ExecStart=node server.js'
    echo 'SuccessExitStatus=143'
    echo '\n'
    echo '[Install]'
    echo 'WantedBy=multi-user.target'
} >> /etc/systemd/system/zap-chat.service

# echo "[ -- Criando arquivo para proxy reverso do projeto com NGINX]"
# {
#     echo 'server {'
#     echo '  server_name dev.zap-chat.com;'
#     echo '  location / {'
#     echo '      proxy_pass http://127.0.0.1:8080;'
#     echo '  }'
#     echo '}'
# } >> /etc/nginx/sites-available/zap-chat.com.conf

echo "[ -- Inicializando daemons criadas]"
systemctl enable zap-chat.service
systemctl daemon-reload

# echo "[ -- Criando Links Simbólicos para Proxy Reverso]"
# unlink /etc/nginx/sites-enabled/default
# ln -s /etc/nginx/sites-available/zap-chat.com.conf /etc/nginx/sites-enabled/zap-chat.com.conf
# systemctl restart nginx

# Para ajustar o letscncrypt
# https://certbot.eff.org/instructions?ws=nginx&os=ubuntufocal
